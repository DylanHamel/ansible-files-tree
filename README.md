# Structure Ansible files tree

###### Dylan Hamel - <dylan.hamel@protonmail.com>



This repo is a structure for Ansible.

This structure is not unique. You can use another.



It will use the ```./templates/common_router_cisco.j2``` for create a file for routers.



Variables will be find in 

* ```./inventories/core/host_vars/{{inventory_hostname}}.yml```
* ```./inventories/core/group_vars/{{group_name}}.yml```



You can run ansible ```cd ./inventories/core/```

```bash
➜ ansible-playbook -i core_hosts playbooks/generate_yaml_config_files.yml

PLAY [cisco-rt] *****************************************************************************************************************************

TASK [template] *****************************************************************************************************************************
changed: [dh-01-rt-001]
changed: [dh-01-rt-002]

PLAY RECAP **********************************************************************************************************************************
dh-01-rt-001               : ok=1    changed=1    unreachable=0    failed=0
dh-01-rt-002               : ok=1    changed=1    unreachable=0    failed=0
```

You will find the following file in ```./configs/```

* dh-01-rt-001.txt
* dh-01-rt-002.txt

```
service password-encryption
hostname dh-01-rt-001

username cisco password admin
username cisco privilege 15

ip cef
no ip domain lookup
ip domain name dh.local


int fa0/0
 description WAN-Access
  ip add 10.0.3.101 255.255.255.0 
 no shut
int fa0/0.10
 description LAN-Access
  encapsulation dot1q 10
  ip add 192.168.10.1 255.255.255.0 
 no shut
int Loopback0
 description Router-ID
  ip add 100.1.1.1 255.255.255.0 
 no shut

router ospf 4
 router-id 100.1.1.1
 log-adjacency-changes
 passive-interface default
  no passive-interface fa0/0
  no passive-interface fa0/0.10
   network 10.0.3.0 0.0.0.255 area 0
  network 192.168.10.0 0.0.0.255 area 0
```



